FROM arm64v8/debian:bookworm
RUN apt update

# DEPS
RUN apt install -y vim python3 gcc python2 g++ make build-essential git git-lfs libffi-dev libssl-dev libglib2.0-0 libnss3 libatk1.0-0 libatk-bridge2.0-0 libx11-xcb1 libgdk-pixbuf-2.0-0 libgtk-3-0 libdrm2 libgbm1 ruby ruby-dev curl wget clang llvm lld clang-tools generate-ninja ninja-build pkg-config tcl wget
RUN gem install fpm
ENV USE_SYSTEM_FPM=true
RUN mkdir -p /usr/include/aarch64-linux-gnu/
# pulled from https://raw.githubusercontent.com/node-ffi-napi/node-ffi-napi/master/deps/libffi/config/linux/arm64/fficonfig.h because its not in debian
COPY fficonfig.h /usr/include/aarch64-linux-gnu/ 
COPY rustup-init /rustup-init
RUN chmod +x /rustup-init
RUN /rustup-init -y

# Buildscripts
COPY signal-buildscript.sh /
RUN chmod +x /signal-buildscript.sh

# Clone signal
RUN git clone https://github.com/signalapp/Signal-Desktop -b 5.30.x
RUN git clone https://github.com/scottnonnenberg-signal/node-sqlcipher -b updates /sqlcipher
COPY patches/sqlcipher.patch /
COPY patches/0001-Remove-no-sandbox-patch.patch /
COPY patches/0001-Minimize-gutter-on-small-screens.patch /
COPY patches/0001-reinstall-cross-deps-on-non-darwin-platforms.patch /
RUN git clone https://github.com/signalapp/libsignal-client.git --branch "v$(grep '"@signalapp/signal-client"' Signal-Desktop/package.json | awk '{ print $2 }' | tr -d '",')"
RUN git clone https://github.com/signalapp/signal-ringrtc-node.git
COPY libringrtc-arm64.node /signal-ringrtc-node/build/linux/libringrtc-arm64.node
RUN git clone https://github.com/signalapp/better-sqlite3.git
COPY patches/better-sqlite3.patch /

# NODE
# Goes last because docker build can't cache the tar.
# https://nodejs.org/dist/v14.15.5/
RUN wget https://nodejs.org/dist/v16.9.1/node-v16.9.1-linux-arm64.tar.gz -O /opt/node-v16.9.1-linux-arm64.tar.gz
RUN mkdir -p /opt/node
RUN cd /opt/; tar xf node-v16.9.1-linux-arm64.tar.gz
RUN mv /opt/node-v16.9.1-linux-arm64/* /opt/node/
ENV PATH=/opt/node/bin:$PATH
RUN npm install --global yarn
RUN npm install --global grunt-cli

#
RUN npm pack "@signalapp/signal-client@$(grep '"@signalapp/signal-client"' Signal-Desktop/package.json | awk '{ print $2 }' | tr -d '",')"
RUN tar xvf signalapp-signal-client-*.tgz
RUN mv package signal-client
