# Signal Desktop Builder
This project allows building Signal Desktop for Debian 11 on ARM64.
It is currently a work in progress, with the goal of building a flatpak
which provides Signal Desktop.

Signed releases are available at https://gitlab.com/undef1/signal-desktop-builder/-/packages

## Current Status:
* [x] Signal Desktop building
* [x] libsignal-client builds
    * Force update neon dependancy.
* [x] zkgroup builds
    * No patching required. Current release builds fine on arm64
* [x] ringrtc builds (note: this is done on the host)
* [x] Bundle zxgroup with Signal-Desktop directory output
* [x] Bundle ringrtc with Signal-Desktop directory output
* [x] flatpak
* [x] Wayland

## Dependancies
This system requires the following:
* Docker or Podman
* qemu-user-static (Debian package)

## Usage
1. Build ringrtc in the ringrtc directory.
2. Build the docker container: `sudo docker build .`
3. Shell into the docker container: `sudo docker run -it <image> bash`
4. Run the build script: `bash /signal-buildscript.sh`
5. Copy the output application from the container (from outside container): `sudo docker cp <container>:/Signal-Desktop/release/<output folder> .`
6. Copy to your Debian Arm64 device.

### Note about Docker
Docker has a concept of "images" and "containers". `docker build` will build an image, `docker run` will create a live container from that image.
The final line of the `docker build` output is the docker "image" hash that you need for `docker run`. The "container" hash will be displayed on the commandline of the container at runtime.
This "container" hash is the one which should be used with `docker cp`.

## Debian Package
To build a Debian package follow to step 4 in usage above, then:
1. (inside the container) change directory to `/Signal-Desktop`
2. Run `yarn build:release --arm64 --linux deb`
3. (On the host) Run `sudo docker cp <container>:/Signal-Desktop/release .`
4. Copy the .deb file in the release folder to your Debian Arm64 device.

Debian packages are also available in this repository's packages section.

## Flatpak

To create and install the flatpak:

 - Install `git`, `flatpak`, and `flatpak-builder` from your OS:
    - Mobian/Debian: `sudo apt install flatpak flatpak-builder`
    - Arch/Manjaro: `sudo pacman -Syu flatpak flatpak-builder`
    - PostmarketOS: `sudo apk add flatpak flatpak-builder`
 - `git clone https://gitlab.com/undef1/signal-desktop-builder && cd signal-desktop-builder`
 - `sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`
 - `sudo flatpak install flathub org.electronjs.Electron2.BaseApp//21.08 org.freedesktop.Platform//21.08 org.freedesktop.Sdk//21.08`
 - `mkdir builddir`
 - `flatpak-builder --user --install --force-clean ./builddir flatpak.yml`

To enable Signal to use a dark theme on GTK-based desktop environments: `sudo flatpak install adwaita-dark -y`

Signal desktop icon should appear in your desktop launcher.

Flatpak builds are available as a Flatpak Repository at https://elagost.com/flatpak built and hosted by @elagost.

## Launcher
The included `signal` shell script provides both a launcher and sandboxing with Bubblewrap.

## RingRTC
RingRTC needs to be built outside the container or x86. To do this, run `setup.sh` and `ringrtc-buildscript.sh` from the ringrtc directory before building signal-desktop. This will build the library and put it in the right place for the docker build. See `ringrtc/README.md` for more information.

## Updates
I recommend that you back up `~/.config/Signal` before updating. While the releases here are tested, I have had a few of them corrupt this folder in the past. If the update fails for any reason, you will need the original database to roll back.

## Distribution Support
This build system targets Mobian Bookworm. However, builds for Debian Bullseye based distributions (PureOS Byzantium) can be created by changing the Docker file to start with `FROM arm64v8/debian:bullseye`. 

Such builds will be uploaded to the packages section from time to time as `<version>_bullseye`. These builds will not work on Mobian Bookworm and vice-versa.

These Bullseye builds are untested, but should work.

## See also:
https://github.com/lsfxz/ringrtc/tree/aarch64  
https://gitlab.com/undef1/Snippets/-/snippets/2100495  
https://gitlab.com/ohfp/pinebookpro-things/-/tree/master/signal-desktop  
Flatpak based on [Flathub Sigal Desktop builds](https://github.com/flathub/org.signal.Signal/)
 - `signal-desktop.sh` https://github.com/flathub/org.signal.Signal/blob/master/signal-desktop.sh
 - `org.signal.Signal.metainfo.xml` https://github.com/flathub/org.signal.Signal/blob/master/org.signal.Signal.metainfo.xml
 - `flatpak.yml` https://github.com/flathub/org.signal.Signal/blob/master/org.signal.Signal.yaml

## Successful builds:
* 5.0.0-beta1
* 5.1.0-beta.5
* 5.5.0-beta.1 - Note: wayland currently broken on this release
* 5.8.0-beta.1
* 5.10.0-beta.1
* 5.14-beta.1
* 5.17-beta.1
* 5.21.0-beta.2 - Note: registration broken, use 5.17 then upgrade
* 5.24.0-beta.1
* 5.28.0-beta.1
* 5.30.0-beta.1
* 5.30.0
