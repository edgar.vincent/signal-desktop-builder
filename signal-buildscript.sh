#!/usr/bin/bash
set -x

# Apply sqlcipher patch to use local (dynamic) libraries
echo "Entering /sqlcipher"
pushd /sqlcipher
patch -Np1 -i ../sqlcipher.patch
popd

# Build and replace arm version of libsignal-client-node
echo "Entering /libsignal-client/node"
pushd /libsignal-client/node
cargo update -p neon
yarn install
sleep 5s # For some reason this file seems to appear a little late.
echo "Entering /libsignal-client-node"
mkdir -p /signal-client/prebuilds/linux-arm64
cp build/Release/libsignal_client_linux_arm64.node /signal-client/prebuilds/linux-arm64/node.napi.node
popd

# Build better-sqlite3 (it's worse)
echo "Entering /better-sqlite3"
pushd /better-sqlite3
# Signal-Desktop has started relying on specific versions from a dev branch...
COMMIT="$(grep '"better-sqlite3"' ../Signal-Desktop/package.json | sed 's:.*better-sqlite3#\(.*\)",:\1:')"
git checkout ${COMMIT}
ln -s /usr/bin/python3 /usr/bin/python
patch -Np1 -i ../better-sqlite3.patch
rm Relase/obj/gen/sqlite3/OpenSSL-Linux/libcrypto.a
npm install node-gyp tar # build depends
npm run build-release
yarn install
popd

# Checkout correct version of ringrtc
pushd /signal-ringrtc-node
COMMIT="$(grep 'signal-ringrtc-node' ../Signal-Desktop/package.json | sed 's:.*signal-ringrtc-node.git#\(.*\)",:\1:')"
git checkout ${COMMIT}
popd

# Signal build requirements
echo "Entering /Signal-Desktop"
pushd /Signal-Desktop
git-lfs install
git am ../0001-Minimize-gutter-on-small-screens.patch
git am ../0001-reinstall-cross-deps-on-non-darwin-platforms.patch
# Drop "--no-sandbox" commit from build
git revert 1ca0d821078286d5953cf0d598e6b97710f816ef
# Dry run
sed -r 's#"@signalapp/signal-client": ".*",#"@signalapp/signal-client": "file:../signal-client",#' -i package.json
sed -r 's#("better-sqlite3": ").*"#\1file:../better-sqlite3"#' -i package.json
sed -r 's#("ringrtc": ").*"#\1file:../signal-ringrtc-node"#' -i package.json
sed -r 's#("zkgroup": ").*"#\1file:../signal-zkgroup-node"#' -i package.json
# This may have to be cancelled and run again to get it to actually rebuild deps...
yarn install # not recommended by signal, but required due to those two sed lines.
yarn install --frozen-lockfile
yarn generate
yarn build:webpack
#yarn test always fails on arm...
yarn build:release --arm64 --linux --dir
popd
